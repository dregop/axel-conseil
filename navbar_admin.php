<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Administration</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <a href="admin_bdd.php"><li><button class="btn btn-large btn-block btn<?php if ($_SESSION["current_form"] != "bdd") { echo("-primary"); } ?> " type="submit">Base de Donnée</button></li></a>
        <a href="admin_testeurs.php"><li><button class="btn btn-large btn-block btn<?php if ($_SESSION["current_form"] != "testeurs") { echo("-primary"); } ?>" type="submit">Testeurs</button></li></a>
        <a href="admin_mail.php"><li><button class="btn btn-large btn-block btn<?php if ($_SESSION["current_form"] != "mails") { echo("-primary"); } ?>" type="submit">Mails</button></li></a>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>