<?php 
    include 'header.php';

    $_SESSION["current_form"] = "motiv_2";
	
	if (!isset($_SESSION['identifiant'])) header('Location: index.php');

	if (!isset($_SESSION['client_email']) OR !isset($_SESSION['id_client'])) header('Location: form_profil.php?new');
	
	if (isset($_POST['comprehension']))
	{			
		if (isset($_SESSION['id_client'],$_SESSION['modifier_client']))
		{
			
			$requete = $bdd->prepare('SELECT id FROM motivation_client WHERE id_client = :id')
									or die(print_r($bdd->errorInfo()));
			$requete->execute(array('id' => $_SESSION['id_client']))
									or die(print_r($bdd->errorInfo()));
			$donnees = $requete->fetch();			
			
			if (isset($donnees['id']))
			{
				$req = $bdd->prepare('UPDATE motivation_client SET comprehension=:comprehension,ancrage=:ancrage,date_test=NOW() WHERE id_client=:id');
				$req->execute(array('comprehension' => $_POST['comprehension'],
									'ancrage' => '',
									'id' => $_SESSION['id_client']));
				$req->closeCursor(); 
			}
			else
			{
				$req = $bdd->prepare('INSERT INTO motivation_client(id_client,comprehension,date_test) 
									VALUES(:id,:comprehension, NOW())');	
				$req->execute(array('id' => $_SESSION['id_client'],
									'comprehension' => $_POST['comprehension']));	
				$req->closeCursor();
			}
			
		}
		elseif (isset($_SESSION['creer_client'],$_SESSION['client_email']))
		{
			$requete = $bdd->prepare('SELECT id FROM client WHERE email = :email')
									or die(print_r($bdd->errorInfo()));
			$requete->execute(array('email' => $_SESSION['client_email']))
									or die(print_r($bdd->errorInfo()));
			$donnees = $requete->fetch();	
			
			if (isset($donnees['id']))
			{			
				$req = $bdd->prepare('INSERT INTO motivation_client(id_client,comprehension,date_test) 
									VALUES(:id,:comprehension, NOW())');	
				$req->execute(array('id' => $donnees['id'],
									'comprehension' => $_POST['comprehension']));	
				$req->closeCursor();
					
				$_SESSION['id_client'] = $donnees['id'];
			}
		}
		else
			header('Location: deconnexion.php');
	}
	elseif(isset($_POST['ancrage']))
	{
		if (isset($_SESSION['id_client'],$_SESSION['modifier_client']))
		{
			$requete = $bdd->prepare('SELECT id FROM motivation_client WHERE id_client = :id')
									or die(print_r($bdd->errorInfo()));
			$requete->execute(array('id' => $_SESSION['id_client']))
									or die(print_r($bdd->errorInfo()));
			$donnees = $requete->fetch();			
			
			if (isset($donnees['id']))
			{
				$req = $bdd->prepare('UPDATE motivation_client SET ancrage=:ancrage,comprehension=:comprehension, date_test=NOW() WHERE id_client=:id');
				$req->execute(array('ancrage' => $_POST['ancrage'],
									'comprehension' => '',
									'id' => $_SESSION['id_client']));
				$req->closeCursor(); 
			}
			else
			{
				$req = $bdd->prepare('INSERT INTO motivation_client(id_client,ancrage,date_test) 
									VALUES(:id,:ancrage, NOW())');	
				$req->execute(array('id' => $_SESSION['id_client'],
									'ancrage' => $_POST['ancrage']));	
				$req->closeCursor();
			}
			
		}
		elseif (isset($_SESSION['creer_client'],$_SESSION['client_email']))
		{
			$requete = $bdd->prepare('SELECT id FROM client WHERE email = :email')
									or die(print_r($bdd->errorInfo()));
			$requete->execute(array('email' => $_SESSION['client_email']))
									or die(print_r($bdd->errorInfo()));
			$donnees = $requete->fetch();	
			
			if (isset($donnees['id']))
			{			
				$req = $bdd->prepare('INSERT INTO motivation_client(id_client,ancrage,date_test) 
									VALUES(:id,:ancrage, NOW())');	
				$req->execute(array('id' => $donnees['id'],
									'ancrage' => $_POST['ancrage']));	
				$req->closeCursor();
					
				$_SESSION['id_client'] = $donnees['id'];
			}
		}
		else
			header('Location: deconnexion.php');
	}
?>

<section>
    <div class="container" style="max-width: 80rem !important;">
        <div class="row-fluid">
            <?php include "nav_deconnexion.php" ?>
            <div class="col-sm-3" style="background-color: #9f9f9f;">
			    <?php include "navbar_motivation.php" ?>
			</div>
		    <div class="col-sm-9">
		        <div class="row-fluid">
		            <div id="title">Deuxième Groupe :</div>
		            <form class="form-horizontal" id="form2" method="post" action="send2.php" style="display:inline-table; width: 100%;">
                        <h1 class="title-center">Projection</h1>
		                <div class="row-fluid" id="flex">
		                    <div class="col-sm-6">
		                        <input type="submit" name="projection" style="margin-bottom: 15px;" class="btn btn-lg btn-block btn-primary" value="Interne" />
		                    </div>
		                    <div class="col-sm-6">
		                        <input type="submit" name="projection" style="margin-bottom: 15px;" class="btn btn-lg btn-block btn-primary" value="Externe" />
		                    </div>
		                </div>
                        <h1 class="title-center">Compétition</h1>
		                <div class="row-fluid" id="flex">
		                    <div class="col-sm-6">
		                        <input type="submit" name="competition" style="margin-bottom: 15px;" class="btn btn-lg btn-block btn-primary" value="Interne" />
		                    </div>
		                    <div class="col-sm-6">
		                        <input type="submit" name="competition" style="margin-bottom: 15px;" class="btn btn-lg btn-block btn-primary" value="Externe" />
		                    </div>
		                </div>
                        <h1 class="title-center">Relationnel</h1>
		                <div class="row-fluid" id="flex">
		                    <div class="col-sm-6">
		                        <input type="submit" name="relationnel" style="margin-bottom: 15px;" class="btn btn-lg btn-block btn-primary" value="Interne" />
		                    </div>
		                    <div class="col-sm-6">
		                        <input type="submit" name="relationnel" style="margin-bottom: 15px;" class="btn btn-lg btn-block btn-primary" value="Externe" />
		                    </div>
		                </div>
		            </form>
		        </div>
		    </div>
        </div>
    </div>
</section>

<?php 
    include 'footer.php';
?>