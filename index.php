<?php 
    include 'header.php';
if (isset($_GET['testeur'],$_SESSION['identifiant']) OR $_SESSION['identifiant'] == 'testeur')
{ // Page d'accueil en tant que Testeur
?> 

<section>
    <div class="container" style="max-width: 80rem !important;">
        <div class="row-fluid">
            <?php include "nav_deconnexion.php" ?>
            <div class="col-sm-12 col-md-6">
                <div class="form">
                    <form action="form_profil.php?new" method="post">
                        <button class="btn btn-large btn-block btn-primary" type="submit">Nouveau client</button>
                    </form>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="form">
                    <form action="search_client.php" method="post">
                        <button class="btn btn-large btn-block btn-primary" type="submit">Modifier client</button>
                    </form>
                </div>
            </div>         
        </div>
    </div>
</section>

<?php
}
elseif (isset($_GET['admin'],$_SESSION['identifiant']) OR $_SESSION['identifiant'] == 'admin')
{ // Page d'accueil en tant qu'administrateur
        
    
    $_SESSION['from_index'] = true;

    include 'admin_bdd.php';

}
if (!isset($_SESSION['identifiant']))
{ 
?>

<section>
    <div class="container" style="max-width: 80rem !important;">
        <div class="row-fluid">
            <div class="col-md-12">
                <div id="title">Bienvenue sur la page d'Authentification :</div>
                <form class="form-horizontal" id="form" method="post" action="index_post.php">
                    <div class="row-fluid">
                        <fieldset>
                            <div class="col-sm-6">
                                <div class="form-group">
                                  <label for="identifiant">Identifiant :</label>
                                  <input type="text" name="identifiant" class="form-control" id="identifiant" placeholder="identifiant">
                                </div>                    
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                  <label for="mot_de_passe">Votre mot de passe  :</label>
                                  <input type="password" name="mot_de_passe" class="form-control" id="mot_de_passe" placeholder="mot de passe">
                                </div>
                            </div>
                            <button type="submit" value="Connexion" class="btn btn-block btn-primary">Connexion</button>
                        </fieldset>
                    </div>
                        <?php
                            // Messages d'erreur
                            if (isset($_GET['erreur']) AND $_GET['erreur'] == 'fail1')
                                echo '<p class="erreur_field">Identifiant ou mot de passe incorrect(s).</p>';
                            if (isset($_GET['erreur']) AND $_GET['erreur'] == 'fail2')
                                echo '<p class="erreur_field">Une erreur est survenue, veuillez rééseiller.</p>';
                        ?>
                </form>
            </div>
        </div>
    </div>
</section>

<?php 
}

    include 'footer.php';
?>