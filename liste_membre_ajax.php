<?php
session_start();
include 'bdd.php';

if (isset($_GET['recherche'],$_GET['type'],$_GET['admin']))
{
	$json['tableau_client'] = '';	
	
	$json['tableau_client'] .= '
                <table class="table table-striped" id="liste2" style="background-color: #a7d2ff;" >
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nom</th>
                          <th>Prenom</th>
                          <th>Statut</th>
                          <th>Testeur</th>
                          <th>Dernière Modification</th>
						  <th></th>
                        </tr>
                      </thead>
                      <tbody>';
									$i = 1;
									if ($_GET['type'] == 'nom_societe')
										$requete = $bdd->prepare('SELECT * FROM client WHERE '.$_GET['type'].' LIKE :recherche OR sport LIKE :recherche 
																ORDER BY '.$_GET['type'].' LIMIT 0, 10')
																or die(print_r($bdd->errorInfo()));
									else
										$requete = $bdd->prepare('SELECT * FROM client WHERE '.$_GET['type'].' LIKE :recherche OR sport LIKE :recherche 
																ORDER BY '.$_GET['type'].' LIMIT 0, 10')
																or die(print_r($bdd->errorInfo()));
						
									$requete->execute(array('recherche' => $_GET['recherche'].'%'))
																or die(print_r($bdd->errorInfo()));																			
									while($donnees = $requete->fetch())
									{	
										// donnees testeur
										$req2 = $bdd->prepare('SELECT identifiant FROM testeur WHERE id=:id')
																	or die(print_r($bdd->errorInfo()));
										$req2->execute(array('id' => $donnees['id_testeur']))
																	or die(print_r($bdd->errorInfo()));	
										$donnees2 = $req2->fetch();
										// fin 
										$json['tableau_client'] .='
											<tr>
											  <th scope="row">'.$i.'</th>';
											  if (strlen($donnees['nom'])>10)
												$json['tableau_client'] .='<td>'.substr($donnees['nom'], 0, 10).'...</td>';
											else	
												$json['tableau_client'] .='<td>'.$donnees['nom'].'</td>';
											if (strlen($donnees['prenom'])>10)
												$json['tableau_client'] .='<td>'.substr($donnees['prenom'], 0, 10).'...</td>';
											else	
												$json['tableau_client'] .='<td>'.$donnees['prenom'].'</td>';
											  
												$json['tableau_client'] .='<td>'.$donnees['statut'].'</td>';
												
										$json['tableau_client'] .=' <td>'.$donnees2['identifiant'].'</td>
											  <td>'.$donnees['date_test'].'</td>
											  <td><a onclick ="var sup=confirm(\'Êtes vous sur de vouloir supprimer ce client ?\');
													if (sup == 0)return false;" 
													href="admin_bdd.php?id='.$donnees['id'].'&supprimer">
												<button class="btn btn-large btn-block btn-danger" type="submit">Supprimer</button>
											</a></td>
											</tr>';
										$i++;
									}
									if ($i == 1)
										$json['tableau_client'] .= 'Aucun client ne correspond à votre recherche.';
                     $json['tableau_client'] .= '</tbody>
                </table>';
					
	// Encodage de la variable tableau json et affichage
	echo json_encode($json);
}
elseif (isset($_GET['recherche'],$_GET['type']))
{
	$json['tableau_client'] = '';	
	
	$json['tableau_client'] .= '
	<table class="table table-striped" id="liste" >
		 <thead>
			<tr>
				  <th>#</th>
				  <th>Nom</th>
				  <th>Prenom</th>
				  <th>Société/Sport</th>
				  <th>Dernière Modification</th>
				  <th></th>
			</tr>
		  </thead>
		<tbody>';	
		
	if ($_GET['type'] == 'nom_societe')
		$requete = $bdd->prepare('SELECT * FROM client WHERE '.$_GET['type'].' LIKE :recherche OR sport LIKE :recherche 
						ORDER BY '.$_GET['type'].' LIMIT 0, 10')
						or die(print_r($bdd->errorInfo()));
	else
		$requete = $bdd->prepare('SELECT * FROM client WHERE '.$_GET['type'].' LIKE :recherche OR sport LIKE :recherche 
						ORDER BY '.$_GET['type'].' LIMIT 0, 10')
						or die(print_r($bdd->errorInfo()));
						
	$requete->execute(array('recherche' => $_GET['recherche'].'%'))
						or die(print_r($bdd->errorInfo()));
	while ($donnees = $requete->fetch()) 
	{	
									  
				 $i = 1;							
					$json['tableau_client'] .= '
						<tr class="client">
						  <th scope="row">'.$i.'</th>';
						  
											  if (strlen($donnees['nom'])>10)
												$json['tableau_client'] .='<td>'.substr($donnees['nom'], 0, 10).'...</td>';
											else	
												$json['tableau_client'] .='<td>'.$donnees['nom'].'</td>';
											if (strlen($donnees['prenom'])>10)
												$json['tableau_client'] .='<td>'.substr($donnees['prenom'], 0, 10).'...</td>';
											else	
												$json['tableau_client'] .='<td>'.$donnees['prenom'].'</td>';
											  
												$json['tableau_client'] .='<td>'.$donnees['statut'].'</td>';
										
					$json['tableau_client'] .= '	
					 <td>'.$donnees['date_test'].'</td>
						  <td><a href="form_profil.php?id='.$donnees['id'].'">
							<button class="btn btn-large btn-block btn-primary" type="submit">Modifier</button>
						  </a></td>
					</tr>';
					$i++;				
	}
			$json['tableau_client'] .= '
			</tbody>
		</table>';		
		
if ($i == 1)
	$json['tableau_client'] .= 'Aucun client ne correspond à votre recherche.';
					
	// Encodage de la variable tableau json et affichage
	echo json_encode($json);
}