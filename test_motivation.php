<?php 
    include 'header.php';

    $_SESSION["current_form"] = "test_motivation";
	
	if (!isset($_SESSION['client_email']) OR !isset($_SESSION['id_client'])) header('Location: form_profil.php?new&cree');	
	
	if (!isset($_SESSION['identifiant'])) header('Location: index.php');
	
?>

<section>
    <div class="container" style="max-width: 80rem !important;">
        <div class="row-fluid">
            <?php include "nav_deconnexion.php" ?>
            <div class="col-sm-3" style="background-color: #9f9f9f;">
                <?php include "navbar_motivation.php" ?>
            </div>
            <div class="col-sm-9">
                <div class="row-fluid">
                    <div id="title">Premier Groupe :</div>
                    <form class="form-horizontal" id="form2" method="post" action="motiv_2.php" style="display:inline-table; width: 100%;">
                        <h1 class="title-center">Compréhension</h1>
                        <div class="row-fluid" id="flex">
                            <div class="col-sm-6">
                                <input type="submit" name="comprehension" style="margin-bottom: 15px;" class="btn btn-lg btn-block btn-primary" value="Interne" />
                            </div>
                            <div class="col-sm-6">
                                <input type="submit" name="comprehension" style="margin-bottom: 15px;" class="btn btn-lg btn-block btn-primary" value="Externe" />
                            </div>
                        </div>
                        <h1 class="title-center">Ancrage</h1>
                        <div class="row-fluid" id="flex">
                            <div class="col-sm-6">
                                <input type="submit" name="ancrage" style="margin-bottom: 15px;" class="btn btn-lg btn-block btn-primary" value="Interne" />
                            </div>
                            <div class="col-sm-6">
                                <input type="submit" name="ancrage" style="margin-bottom: 15px;" class="btn btn-lg btn-block btn-primary" value="Externe" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<?php 
    include 'footer.php';
?>