<?php
    include 'header.php';	
	
		
	$requete = $bdd->prepare('SELECT * FROM client WHERE email = :email')
							or die(print_r($bdd->errorInfo()));
	$requete->execute(array('email' => $_SESSION['client_email']))
							or die(print_r($bdd->errorInfo()));
	$donnees = $requete->fetch();
?>
<section>
    <div class="container" style="max-width: 80rem !important;">
        <div class="row-fluid">
        <?php include "nav_deconnexion.php" ?>
            <div class="col-sm-3" style="background-color: #9f9f9f;">
		        <?php include "navbar_profil.php" ?>
		    </div>
            <div class="col-md-9">
	            <div id="title" >Récapitulatif des données enregistrées dans la base de donnée :</div>
				<div id="form">
					Nom :         <?php if (!empty($donnees['nom']))           echo $donnees['nom']; ?>   
					| Prenom :    <?php if (!empty($donnees['prenom']))        echo $donnees['prenom']; ?> <br />
					Email :       <?php if (!empty($donnees['email']))         echo $donnees['email']; ?>
					| Telephone : <?php if (!empty($donnees['telephone']))     echo $donnees['telephone'];     else echo 'Non renseigné.'; ?> <br />
					Statut :      <?php if (!empty($donnees['statut']))        echo $donnees['statut']; ?>
					| Sport :     <?php if (!empty($donnees['sport']))         echo $donnees['sport'];         else echo 'Non renseigné.'; ?> <br />
					Société :     <?php if (!empty($donnees['nom_societe']))   echo $donnees['nom_societe'];   else echo 'Non renseigné.'; ?>
					| Poste :     <?php if (!empty($donnees['poste_societe'])) echo $donnees['poste_societe']; else echo 'Non renseigné.'; ?> <br />
					Departement : <?php if (!empty($donnees['departement']))   echo $donnees['departement'];   else echo 'Non renseigné.'; ?>
				</div>
            </div>
        </div>
    </div>
</section>

<?php 
    include 'footer.php';
?>