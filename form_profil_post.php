<?php


include 'header.php';

// on enregistre les données de la page form_profil
if (isset($_POST['nom'],$_POST['prenom'],$_POST['email'],$_POST['statut'],$_SESSION['id_testeur'])
	AND $_POST['nom'] != '' AND $_POST['prenom'] != '' AND $_POST['email'] != '')
{	
	// on verfie si les champs obligatoires sont remplis
	if (($_POST['statut'] == 'Professionel' AND isset($_POST['nom_societe'])
	AND $_POST['nom_societe'] != '') OR
	($_POST['statut'] == 'Sportif' AND isset($_POST['sport']) AND $_POST['sport'] != '') OR
	($_POST['statut'] == 'Particulier'))
	{	
		// verif email existant
		$requete2 = $bdd->prepare('SELECT email FROM client WHERE email = :email')
								or die(print_r($bdd->errorInfo()));
		$requete2->execute(array('email' => $_POST['email']))
								or die(print_r($bdd->errorInfo()));
		$donnees2 = $requete2->fetch();	
		if (isset($donnees2['email']) AND !empty($donnees2['email']))
			header('Location: form_profil.php?new&erreur=email_existant');
		else
		{
			$req = $bdd->prepare('INSERT INTO client(nom, prenom, email, statut, telephone, nom_societe,
								poste_societe, sport, departement, id_testeur, date_test) 
								VALUES(:nom, :prenom, :email, :statut, :telephone, :nom_societe,
								:poste_societe, :sport, :departement, :id_testeur, NOW())');
			$req->execute(array('nom' => $_POST['nom'],
								'prenom' => $_POST['prenom'],
								'email' => $_POST['email'],
								'statut' => $_POST['statut'],
								'telephone' => $_POST['telephone'],
								'nom_societe' => $_POST['nom_societe'],
								'poste_societe' => $_POST['poste_societe'],
								'sport' => $_POST['sport'],
								'departement' => $_POST['departement'],
								'id_testeur' => $_SESSION['id_testeur']));
			$req->closeCursor(); // Termine le traitement de la requète	
				
			$_SESSION['client_email'] = $_POST['email'];
			$_SESSION['nom'] = $_POST['nom']; $_SESSION['prenom'] = $_POST['prenom'];
			$_SESSION['telephone'] = $_POST['telephone']; $_SESSION['statut'] = $_POST['statut'];
			$_SESSION['nom_societe'] = $_POST['nom_societe']; $_SESSION['poste_societe'] = $_POST['poste_societe']; 
			$_SESSION['sport'] = $_POST['sport']; $_SESSION['departement'] = $_POST['departement'];		

			header('Location: recap.php');
		}
			
	}
	else
		header('Location: form_profil.php?erreur=incomplet');	
}
// si c'est pour modifier
elseif(isset($_SESSION['modifier_client'],$_SESSION['id_client']) OR isset($_SESSION['client_email']))
{
	if (isset($_SESSION['id_client']))	
	{					
		$requete2 = $bdd->prepare('SELECT email FROM client WHERE id = :id')
							or die(print_r($bdd->errorInfo()));
		$requete2->execute(array('id' => $_SESSION['id_client']))
							or die(print_r($bdd->errorInfo()));
		$donnees2 = $requete2->fetch();	
		$_SESSION['client_email'] = $donnees2['email'];
	}
	$req = $bdd->prepare('UPDATE client SET nom=:nom, prenom=:prenom,
						statut=:statut, telephone=:telephone, nom_societe=:nom_societe,
						poste_societe=:poste_societe, sport=:sport, departement=:departement, 
						id_testeur=:id_testeur, date_test=NOW() WHERE email=:email');
	$req->execute(array('nom' => $_POST['nom'],
					'prenom' => $_POST['prenom'],
					'email' => $_SESSION['client_email'],
					'statut' => $_POST['statut'],
					'telephone' => $_POST['telephone'],
					'nom_societe' => $_POST['nom_societe'],
					'poste_societe' => $_POST['poste_societe'],
					'sport' => $_POST['sport'],
					'departement' => $_POST['departement'],
					'id_testeur' => $_SESSION['id_testeur']));
	$req->closeCursor(); 
					
	$_SESSION['nom'] = $_POST['nom']; $_SESSION['prenom'] = $_POST['prenom'];
	$_SESSION['telephone'] = $_POST['telephone']; $_SESSION['statut'] = $_POST['statut'];
	$_SESSION['nom_societe'] = $_POST['nom_societe']; $_SESSION['poste_societe'] = $_POST['poste_societe']; 
	$_SESSION['sport'] = $_POST['sport']; $_SESSION['departement'] = $_POST['departement'];	
					
	header('Location: recap.php');
				
}
else
	header('Location: form_profil.php?erreur=incomplet');	