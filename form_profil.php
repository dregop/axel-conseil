<?php 
    include 'header.php';
	
	if (!isset($_SESSION['identifiant'])) header('Location: index.php');
	
    $_SESSION["current_form"] = "profil";
	
	// SI C'est un nouveau client
	if (isset($_GET['new'])) 
	{
		unset($_SESSION['modifier_client']); 
		unset($_SESSION['nom']); 
		unset($_SESSION['prenom']); 
		unset($_SESSION['telephone']); 
		unset($_SESSION['statut']); 
		unset($_SESSION['client_email']); 
		unset($_SESSION['nom_societe']); 
		unset($_SESSION['poste_societe']); 
		unset($_SESSION['sport']); 
		unset($_SESSION['departement']); 
		
		$_SESSION['creer_client'] = 'on';
	}
	
	// Si on modifie un client
	if (isset($_GET['id']))
	{
		$_SESSION['id_client'] = $_GET['id'];
		$_SESSION['modifier_client'] = 'on';

		$requete = $bdd->prepare('SELECT * FROM client WHERE id=:id')
						or die(print_r($bdd->errorInfo()));						
		$requete->execute(array('id' => $_SESSION['id_client']))
						or die(print_r($bdd->errorInfo()));
		$donnees = $requete->fetch();

		// création sessions
		$_SESSION['nom']           = $donnees['nom'];
        $_SESSION['prenom']        = $donnees['prenom'];
		$_SESSION['telephone']     = $donnees['telephone'];
        $_SESSION['statut']        = $donnees['statut'];
		$_SESSION['client_email']  = $donnees['email'];
		$_SESSION['nom_societe']   = $donnees['nom_societe'];
        $_SESSION['poste_societe'] = $donnees['poste_societe']; 
		$_SESSION['sport']         = $donnees['sport'];
        $_SESSION['departement']   = $donnees['departement'];
	}

	if (isset($_GET['cree']))
	{
?>
	<div id="overlay">
		Vous devez créér le client pour faire les tests     
		<a href="#" class="btn btn-primary" id="fermer_overlay" style="color:black;"> OK </a>
	</div>
<?php
	}
	elseif (isset($_GET['erreur']))
	{
?>
	<div id="overlay">
		<?php 
		if ($_GET['erreur'] == 'incomplet') 
		{ ?>
		Les champs obligatoires n'ont pas été remplis  
		<a href="#" class="btn btn-primary"  id="fermer_overlay" style="color:black;"> OK </a>
		<?php
		}
		elseif ($_GET['erreur'] == 'email_existant')
		{
		?>
		Email déjà existant 
		<a href="#" class="btn btn-primary"  id="fermer_overlay" style="color:black;"> OK </a>
		<?php
		}
		?>
	</div>
<?php
	}
	elseif (isset($_GET['donnees_envoyees']))
	{
		?>
		<div id="overlay">
		Les données ont bien été envoyées 
		<a href="#" class="btn btn-primary"  id="fermer_overlay" style="color:black;"> OK </a>
		</div>
		<?php
	}

?>
<section>
<div class="container" style="max-width: 80rem !important;">
  <div class="row-fluid">
  <?php include "nav_deconnexion.php" ?>
    <div class="col-sm-3" style="background-color: #9f9f9f;">
        <?php include "navbar_profil.php" ?>
    </div>
    <div class="col-sm-9">
        <div class="row-fluid">
            <div id="title">Renseigner le profil client:</div>
            <form class="form-horizontal" id="form" method="post" action="form_profil_post.php">
                <div class="row-fluid">
                    <fieldset>
                        <legend>Informations personnelles : </legend>
                        <div class="col-sm-6">
                            <div class="form-group">
                              <label for="nom">Nom :</label>
									<input type="text" name="nom" required class="form-control" id="nom" placeholder="Votre nom" 
									<?php if (isset($_SESSION['nom'])) echo'value="'.$_SESSION['nom'].'"';?>>
                            </div>                    
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                              <label for="prenom">Prenom :</label>
									<input type="text" name="prenom" required class="form-control" id="prenom" placeholder="Votre prénom" 
									<?php if (isset($_SESSION['prenom'])) echo'value="'.$_SESSION['prenom'].'"';?>>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                              <label for="age">E-mail (Une fois enregisté, l'email est non modifiable) :</label>
									  <input type="text" name="email" required class="form-control" id="e-mail" placeholder="Votre E-mail"
									  <?php if (isset($_SESSION['client_email'])) echo'value="'.$_SESSION['client_email'].'" disabled="disabled"';?>>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                              <label for="telephone">Numéro de téléphone :</label>
									<input type="text" name="telephone" class="form-control" id="telephone" placeholder="Votre numéro de téléphone"
									<?php if (isset($_SESSION['telephone'])) echo'value="'.$_SESSION['telephone'].'"';?>>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Statut :</label>
                                <select name="statut" class="form-control">
                                  <option <?php if (isset($_SESSION['statut']) AND $_SESSION['statut'] == 'Professionel') 
										echo'selected="selected"'?> value="Professionel">Professionel</option>
                                  <option <?php if (isset($_SESSION['statut']) AND $_SESSION['statut'] == 'Sportif')
										echo'selected="selected"'?> value="Sportif">Sportif</option>
                                  <option <?php if (isset($_SESSION['statut']) AND $_SESSION['statut'] == 'Particulier')
										echo'selected="selected"'?> value="Particulier">Particulier</option>
                                </select>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                    <legend>Informations Professionnel : </legend>
                        <div class="col-sm-6">
                            <div class="form-group">
                              <label for="nom_societe">Nom de votre société :</label>
									<input type="text" name="nom_societe" class="form-control" id="nom_societe" placeholder="Le nom de votre société"
									<?php if (isset($_SESSION['nom_societe'])) echo'value="'.$_SESSION['nom_societe'].'"';?>>
                            </div>                    
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                              <label for="poste_societe">Poste occupé dans votre société : (Facultatif)</label>
									<input type="text" required name="poste_societe" class="form-control" id="poste_societe" placeholder="Poste dans la société"
									<?php if (isset($_SESSION['poste_societe'])) echo'value="'.$_SESSION['poste_societe'].'"';?>>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                    <legend>Autre : </legend>
                        <div class="col-sm-6">
                            <div class="form-group">
                              <label for="sport">Sport que vous pratiquez :</label>
									<input type="text" name="sport" class="form-control" id="sport" placeholder="Sport"
									<?php if (isset($_SESSION['sport'])) echo'value="'.$_SESSION['sport'].'"';?>>
                            </div>                    
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                              <label for="departement">Département : (Facultatif)</label>
                              <input type="text" name="departement" class="form-control" id="departement" placeholder="Département"
							  <?php if (isset($_SESSION['departement'])) echo'value="'.$_SESSION['departement'].'"';?>>
                            </div>
                        </div>
                    </fieldset>
                    <button type="submit" class="btn btn-block btn-primary">Terminer</button>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
</section>

<script>			
	document.getElementById('fermer_overlay').onclick = function() {
		document.getElementById('overlay').style.display = "none";
		return false;
	};
</script>

<?php 
    include 'footer.php';
?>