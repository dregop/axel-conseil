<?php 
    include 'header.php';

	if (!isset($_SESSION['client_email'])) header('Location: form_profil.php?new');		
	
	if (isset($_POST['P']) OR isset($_POST['J']))
	{
		if (isset($_POST['P']))
			$profil = $_POST['P'];
		else
			$profil = $_POST['J'];
			
		if (isset($_SESSION['id_client']))
		{
			$requete = $bdd->prepare('SELECT id FROM profil_client WHERE id_client = :id')
									or die(print_r($bdd->errorInfo()));
			$requete->execute(array('id' => $_SESSION['id_client']))
									or die(print_r($bdd->errorInfo()));
			$donnees = $requete->fetch();			
			
			if (isset($donnees['id']))
			{
				$req = $bdd->prepare('UPDATE profil_client SET p_j=:p_j,date_test=NOW() WHERE id_client=:id');
				$req->execute(array('p_j' => $profil,
									'id' => $_SESSION['id_client']));
				$req->closeCursor(); 
			}
			else
			{
				$req = $bdd->prepare('INSERT INTO profil_client(id_client,p_j,date_test) 
									VALUES(:id,:p_j, NOW())');	
				$req->execute(array('id' => $_SESSION['id_client'],
									'p_j' => $profil));	
				$req->closeCursor();
			}
			
		}
		else
			header('Location: form_i_e.php?erreur=debut');
	}	
	
if (isset($_GET['donnees_envoyees'],$_SESSION['id_client']))
{
?>
<section>
    <div class="container" style="max-width: 80rem !important;">
        <div class="row-fluid">
        <?php include "nav_deconnexion.php" ?>
            <div id="title" class="col-md-12">
                Les données ont bien été envoyés par mail au client
            </div>
            <h2 style="text-align:center; margin-bottom:10%; padding:10px;">Redirection vers la page principale dans 3 secondes.</h2>
        </div>
    </div>
</section>

<?php 
    header( "Refresh:1; url=index.php", true, 303);
}
elseif (isset($_SESSION['id_client']))
{
	$requete = $bdd->prepare('SELECT * FROM profil_client WHERE id_client = :id')
							or die(print_r($bdd->errorInfo()));
	$requete->execute(array('id' => $_SESSION['id_client']))
							or die(print_r($bdd->errorInfo()));
	$donnees = $requete->fetch();
?>
<section>
    <div class="container" style="max-width: 80rem !important;">
        <div class="row-fluid">
        <?php include "nav_deconnexion.php" ?>
            <div id="title" class="col-md-12">
                Récapitulatif des données enregistrées dans la base de donnée :
            </div>
			<div id="form">
				<?php 
				if (!empty($donnees['i_e'])) echo 'I ou E : '.$donnees['i_e'].' <br />';
				if (!empty($donnees['n_s']))       echo 'N ou S : '.$donnees['n_s'].' <br />';
				if (!empty($donnees['t_f']))    echo 'T ou F :  '.$donnees['t_f'].' <br />';
				if (!empty($donnees['p_j']))   echo 'P ou J : '.$donnees['p_j'].' <br />'; 
				?>				
			</div>	
            <form class="form-horizontal" id="form2" method="post" action="send_post.php">
                <div class="col-sm-12">
                    <button type="submit" name="mail" class="btn btn-lg btn-block btn-primary">Envoyer</button>
                </div>
            </form>			
        </div>
    </div>
</section>
<?php
}
else
	header('Location: form_profil.php?new');

   include 'footer.php';
