<?php 
    include 'header.php';

	if (!isset($_SESSION['identifiant'])) header('Location: index.php');

	if (!isset($_SESSION['client_email'])) header('Location: form_profil.php?new');
	
	if (isset($_POST['I']) OR isset($_POST['E']))
	{
		if (isset($_POST['I']))
			$profil = $_POST['I'];
		else
			$profil = $_POST['E'];
			
		if (isset($_SESSION['id_client']))
		{
			$requete = $bdd->prepare('SELECT id FROM profil_client WHERE id_client = :id')
									or die(print_r($bdd->errorInfo()));
			$requete->execute(array('id' => $_SESSION['id_client']))
									or die(print_r($bdd->errorInfo()));
			$donnees = $requete->fetch();			
			
			if (isset($donnees['id']))
			{
				$req = $bdd->prepare('UPDATE profil_client SET i_e=:i_e,date_test=NOW() WHERE id_client=:id');
				$req->execute(array('i_e' => $profil,
									'id' => $_SESSION['id_client']));
				$req->closeCursor(); 
			}
			else
			{
				$req = $bdd->prepare('INSERT INTO profil_client(id_client,i_e,date_test) 
									VALUES(:id,:i_e, NOW())');	
				$req->execute(array('id' => $_SESSION['id_client'],
									'i_e' => $profil));	
				$req->closeCursor();
			}
			
		}
		elseif (isset($_SESSION['client_email']))
		{
			$requete = $bdd->prepare('SELECT id FROM client WHERE email = :email')
									or die(print_r($bdd->errorInfo()));
			$requete->execute(array('email' => $_SESSION['client_email']))
									or die(print_r($bdd->errorInfo()));
			$donnees = $requete->fetch();	
			
			if (isset($donnees['id']))
			{			
				$req = $bdd->prepare('INSERT INTO profil_client(id_client,i_e,date_test) 
									VALUES(:id,:i_e, NOW())');	
				$req->execute(array('id' => $donnees['id'],
									'i_e' => $profil));	
				$req->closeCursor();
					
				$_SESSION['id_client'] = $donnees['id'];
			}
		}
		else
			header('Location: form_profil.php?new');
	}

    $_SESSION["current_form"] = "n_s";
?>
<section>
<div class="container" style="max-width: 80rem !important;">
  <div class="row-fluid">
    <?php include "nav_deconnexion.php" ?>
    <div class="col-sm-3" style="background-color: #9f9f9f;">
        <?php include "navbar_profil.php" ?>
    </div>
    <div class="col-sm-9">
        <div class="row-fluid">
            <div id="title">Choisir la catégorie :</div>
            <form class="form-horizontal" id="form" method="post" action="form_t_f.php">
                <div class="row-fluid" id="flex">
                    <div class="col-sm-6">
                        <input type="submit" name="N" style="margin-bottom: 15px;" class="btn btn-lg btn-block btn-primary" value="N" />
                    </div>
                    <div class="col-sm-6">
                         <input type="submit" name="S" style="margin-bottom: 15px;" class="btn btn-lg btn-block btn-primary" value="S" />
                    </div>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
</section>
<?php 
    include 'footer.php';
?>