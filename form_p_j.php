<?php 
    include 'header.php';

	if (!isset($_SESSION['identifiant'])) header('Location: index.php');

	if (!isset($_SESSION['client_email'])) header('Location: form_profil.php?new');
	
	if (isset($_POST['T']) OR isset($_POST['F']))
	{
		if (isset($_POST['T']))
			$profil = $_POST['T'];
		else
			$profil = $_POST['F'];
			
		if (isset($_SESSION['id_client']))
		{
			$requete = $bdd->prepare('SELECT id FROM profil_client WHERE id_client = :id')
									or die(print_r($bdd->errorInfo()));
			$requete->execute(array('id' => $_SESSION['id_client']))
									or die(print_r($bdd->errorInfo()));
			$donnees = $requete->fetch();			
			
			if (isset($donnees['id']))
			{
				$req = $bdd->prepare('UPDATE profil_client SET t_f=:t_f,date_test=NOW() WHERE id_client=:id');
				$req->execute(array('t_f' => $profil,
									'id' => $_SESSION['id_client']));
				$req->closeCursor(); 
			}
			else
			{
				$req = $bdd->prepare('INSERT INTO profil_client(id_client,t_f,date_test) 
									VALUES(:id,:t_f, NOW())');	
				$req->execute(array('id' => $_SESSION['id_client'],
									't_f' => $profil));	
				$req->closeCursor();
			}
			
		}
		else
			header('Location: form_i_e.php?erreur=debut');
	}
    $_SESSION["current_form"] = "p_j";
?>
<section>
<div class="container" style="max-width: 80rem !important;">
  <div class="row-fluid">
    <?php include "nav_deconnexion.php" ?>
    <div class="col-sm-3" style="background-color: #9f9f9f;">
        <?php include "navbar_profil.php" ?>
    </div>
    <div class="col-sm-9">
        <div class="row-fluid">
            <div id="title">Choisir la catégorie :</div>
            <form class="form-horizontal" id="form" method="post" action="send.php">
                <div class="row-fluid" id="flex">
                    <div class="col-sm-6" >
                        <input type="submit" name="P" style="margin-bottom: 15px;" class="btn btn-lg btn-block btn-primary" value="P" />
                    </div>
                    <div class="col-sm-6">
                        <input type="submit" name="J" style="margin-bottom: 15px;" class="btn btn-lg btn-block btn-primary" value="J" />
                    </div>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
</section>
<?php 
    include 'footer.php';
?>