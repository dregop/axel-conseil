<?php 
    include 'header.php';
  if (!isset($_SESSION['identifiant'])) header('Location: index.php');

        $_SESSION["current_form"] = "testeurs";
		
if(isset($_GET['id'],$_GET['supprimer']))
{
	$req = $bdd->prepare('DELETE FROM testeur WHERE id=:id')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id' => $_GET['id'])) 
						or die(print_r($bdd->errorInfo()));
	$req->closeCursor(); 
		
	header('Location: admin_testeurs.php');  
}
?>

<section style="background-color:#7dbdff;" >
    <div class="container" style="max-width: 80rem !important;">
        <div class="row-fluid">
            <?php include "nav_deconnexion.php" ?>
            <div class="col-sm-3" style="background-color: #9f9f9f;">
			    <?php include "navbar_admin.php" ?>
			</div>
            <div class="col-sm-9">
            <div class="row-fluid">
                <form class="form-horizontal" id="form" style="background-color: #a7d2ff; padding: 10px; margin:0;" method="post" action="ajouter_testeur.php">
                    <input type="submit" name="add_testeur" class="btn btn-lg btn-block btn-primary" value="Ajouter un testeur" />
                </form>
            </div>
        </div>
            <div class="col-sm-9">
                <table class="table table-striped" id="liste" style="background-color: #a7d2ff; margin-top: 20px;" >
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Identifiant</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
						 <?php 		
							$i = 1;
							$req = $bdd->query('SELECT * FROM testeur ORDER BY identifiant')
													or die(print_r($bdd->errorInfo()));
							while($donnees = $req->fetch())
							{	
								echo'
									<tr>
										<th scope="row">'.$i.'</th>
											<td>'.$donnees['identifiant'].'</td>
											<td><a onclick ="var sup=confirm(\'Êtes vous sur de vouloir supprimer ce Testeur ?\');
													if (sup == 0)return false;" 
													href="admin_testeurs.php?id='.$donnees['id'].'&supprimer">
												<button class="btn btn-large btn-block btn-danger" type="submit">Supprimer</button>
											</a></td>
									</tr>';
								$i++;
							}
							if ($i == 1)
								echo 'Aucun testeur n\'a été créé, ajoutez en un.';
						?>
                      </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<?php 
    include 'footer.php';
?>