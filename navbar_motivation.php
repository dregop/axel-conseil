<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Barre de Navigation</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <a href="form_profil.php"><li><button class="btn btn-large btn-block btn<?php if ($_SESSION["current_form"] != "profil") { echo("-primary"); } ?> " type="submit">Informations</button></li></a>
        <a href="form_i_e.php"><li><button class="btn btn-large btn-block btn<?php

         if ($_SESSION["current_form"] != "i_e"
         && $_SESSION["current_form"] != "n_s"
         && $_SESSION["current_form"] != "t_f"
         && $_SESSION["current_form"] != "p_j"
         && $_SESSION["current_form"] != "send") { echo("-primary"); } 

         ?> " type="submit">Test Profil</button></li></a>
        <a href="test_motivation.php"><li><button class="btn btn-large btn-block btn<?php if ($_SESSION["current_form"] != "test_motivation"
        && $_SESSION["current_form"] != "motiv_2" ) { echo("-primary"); } ?>" type="submit">Test Motivation</button></li></a>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>