<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Axel Conseil App</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/uploadfile.min.css" rel="stylesheet">
        
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div id="header">
            <div class="container" style=" height: 160px; ">
                <div class="row-fluid">
                        <a href="index.php"><img src="img/logo.png" style="height:135px;" alt="logo"></a>
                    <div id="slogan">La performance individuelle et collective</div>
                </div>
            </div>
        </div>
        <?php 
            error_reporting(0);
			session_start(); 

			include 'bdd.php';
            
		?>