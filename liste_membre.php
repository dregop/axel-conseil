<?php 
    include 'header.php';
?>

<section>
    <div class="container" style="max-width: 80rem !important;">
        <div class="row-fluid">
            <?php include "nav_deconnexion.php" ?>
            <div id="title" class="col-md-12">
                Voici la liste de vos client :
            </div>
            <div class="col-sd-12">
                <table class="table table-striped" id="liste" >
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>First Name</th>
                          <th>Last Name</th>
                          <th>Username</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">1</th>
                          <td>Mark</td>
                          <td>Otto</td>
                          <td>@mdo</td>
                          <td><a href="modif_client.php"><button class="btn btn-large btn-block btn-primary" type="submit">Modifier</button></a></td>
                        </tr>
                        <tr>
                          <th scope="row">2</th>
                          <td>Jacob</td>
                          <td>Thornton</td>
                          <td>@mdo</td>
                          <td><a href="modif_client.php"><button class="btn btn-large btn-block btn-primary" type="submit">Modifier</button></a></td>
                        </tr>
                        <tr>
                          <th scope="row">3</th>
                          <td>Larry</td>
                          <td>the Bird</td>
                          <td>@mdo</td>
                          <td><a href="modif_client.php"><button class="btn btn-large btn-block btn-primary" type="submit">Modifier</button></a></td>
                        </tr>
                      </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<?php 

    include 'footer.php';
?>