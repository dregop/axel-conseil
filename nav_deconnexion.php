<?php 

function displaySessionType()
{
	if ($_SESSION['identifiant'] == 'testeur') 
	{
		echo("Testeur");
	}
	elseif ($_SESSION['identifiant'] == 'admin') 
	{
		echo("Administrateur");
	}
}

?>

<div id="top_connect_bar" class="col-md-12">
    <a href="deconnexion.php" style="float: right;">
    	<button class="btn btn-large btn-danger" type="submit">Déconnexion</button>
    </a>
    <a href="index.php" style="float: left;">
    	<button class="btn btn-large btn-primary" type="submit">Accueil</button>
    </a>
    <p style=" margin-top: 5px; font-size: 16px; ">Connecté en tant que <?php displaySessionType() ?> </p>
</div>