<?php 
    include 'header.php';
	
	if (!isset($_SESSION['identifiant'])) header('Location: index.php');

	if (!isset($_SESSION['client_email'])) header('Location: form_profil.php?new&cree');
	
	$_SESSION["current_form"] = "i_e";
	
	if (isset($_GET['erreur']) AND $_GET['erreur'] == 'debut')
	{
?>
	<div id="overlay">
		Veuillez commencer par le début du test.    
		<a href="#" class="btn btn-primary"  id="fermer_overlay" style="color:black;"> OK </a>
	</div>
<?php
	}
?>
<section>
<div class="container" style="max-width: 80rem !important;">
	<div class="row-fluid">
  		<?php include "nav_deconnexion.php" ?>
	    <div class="col-sm-3" style="background-color: #9f9f9f;">
	        <?php include "navbar_profil.php" ?>
	    </div>
	    <div class="col-sm-9">
	        <div class="row-fluid">
	            <div id="title">Choisir la catégorie :</div>
	            <form class="form-horizontal" id="form" method="post" action="form_n_s.php">
	                <div class="row-fluid" id="flex">
	                    <div class="col-sm-6">
	                        <input type="submit" name="I" style="margin-bottom: 15px;" class="btn btn-lg btn-block btn-primary" value="I" />
	                    </div>
	                    <div class="col-sm-6">
	                        <input type="submit" name="E" class="btn btn-lg btn-block btn-primary" value="E" />
	                    </div>
	                </div>
	            </form>
	        </div>
	    </div>
	</div>
</div>
</section>

<script>			
	document.getElementById('fermer_overlay').onclick = function() {
		document.getElementById('overlay').style.display = "none";
		return false;
	};
</script>

<?php 
    include 'footer.php';
?>