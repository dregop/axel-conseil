<?php 

    if (@$_SESSION["from_index"] != true) 
    {
        include 'header.php';
    }

      if (!isset($_SESSION['identifiant'])) header('Location: index.php');

    $_SESSION["current_form"] = "bdd";

	if(isset($_GET['id'],$_GET['supprimer']))
	{
		$req = $bdd->prepare('DELETE FROM client WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
		$req->execute(array('id' => $_GET['id'])) 
							or die(print_r($bdd->errorInfo()));
		$req->closeCursor(); 
			
		header('Location: admin_bdd.php');  
	}
?>

<section style="background-color:#7dbdff;" >
    <div class="container" style="max-width: 80rem !important;">
        <div class="row-fluid">
            <?php include "nav_deconnexion.php" ?>
            <div class="col-sm-3" style="background-color: #9f9f9f;">
			    <?php include "navbar_admin.php" ?>
			</div>
            <div class="col-sm-9" id="clients">
				<!-- recherche de clients -->
				<form class="form-horizontal" id="form2" method="post" action="" 
				style="background-color: #a7d2ff;display:inline-table; width: 100%;">
					<h3>Rechercher un client :</h3>
					<div class="row-fluid">
						<div class="col-sm-6">
							<div class="form-group">
							    <label for="nom">Nom :</label>
								<input type="text" name="nom" class="form-control" id="nom" placeholder="Nom">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							  <label for="prenom">Prénom :</label>
							  <input type="text" name="prenom" class="form-control" id="prenom" placeholder="Prénom">
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<label for="nom_societe">Le nom de la Société  ou le sport :</label>
								<input type="text" name="nom_societe" class="form-control" id="nom_societe/sport" placeholder="Nom Société">
							</div>
						</div>
					</div>
				</form>	
				
				<div id="clients_recherche">
					<h3 id="titre_recherche">Recherche clients : </h3>
				</div>
				
				<h3>Derniers clients modifiés :</h3>
                <table class="table table-striped" id="liste" style="padding:0px; background-color: #a7d2ff;" >
                      <thead>
                        <tr class="client">
                          <th>#</th>
                          <th>Nom</th>
                          <th>Prenom</th>
                          <th>Statut</th>
                          <th>Testeur</th>
                          <th>Dernière Modification</th>
						  <th></th>
                        </tr>
                      </thead>
                      <tbody>
							    <?php 
									$i = 1;
									$numero_page = 0;
									if (isset($_GET['page']))
									{
										$numero_page = ($_GET['page'] - 1)*8;
										$i = $numero_page + 1;
									}
																			
									$req = $bdd->prepare('SELECT * FROM client ORDER BY date_test DESC LIMIT '.$numero_page.',8')
																or die(print_r($bdd->errorInfo()));
									$req->execute(array('id_to' => $_SESSION['identifiant']))
																or die(print_r($bdd->errorInfo()));
									while($donnees = $req->fetch())
									{	
										// donnees testeur
										$req2 = $bdd->prepare('SELECT identifiant FROM testeur WHERE id=:id')
																	or die(print_r($bdd->errorInfo()));
										$req2->execute(array('id' => $donnees['id_testeur']))
																	or die(print_r($bdd->errorInfo()));	
										$donnees2 = $req2->fetch();
										// fin 
										echo'
											<tr>
											  <th scope="row">'.$i.'</th>';
											  if (strlen($donnees['nom'])>10)
												echo'<td>'.substr($donnees['nom'], 0, 10).'...</td>';
											else	
												echo'<td>'.$donnees['nom'].'</td>';
											if (strlen($donnees['prenom'])>10)
												echo'<td>'.substr($donnees['prenom'], 0, 10).'...</td>';
											else	
												echo'<td>'.$donnees['prenom'].'</td>';
											  
												echo '<td>'.$donnees['statut'].'</td>';
												
										echo' <td>'.$donnees2['identifiant'].'</td>
											  <td>'.$donnees['date_test'].'</td>
											  <td><a onclick ="var sup=confirm(\'Êtes vous sur de vouloir supprimer ce client ?\');
													if (sup == 0)return false;" 
													href="admin_bdd.php?id='.$donnees['id'].'&supprimer">
													<button class="btn btn-large btn-block btn-danger" type="submit">Supprimer</button>
											      </a>
											  </td>
											</tr>';
										$i++;
									}
									if ($i == 1)
										echo 'Aucun client n\'a été enregistré pour le moment.';
								?>
                      </tbody>
                </table>
				<?php
					$nbre_page = 1;
					$p = $bdd->query('SELECT COUNT(id) 
										AS nbr_joueur
										FROM client')
										or die(print_r($bdd->errorInfo()));
					$do = $p->fetch();				
					$nbr_entrees = $do['nbr_joueur'];
				
					if (isset ($_GET['page']))
						$current_page = $_GET['page'];
					else
						$current_page = 1;
					
					$nbr_affichage = 8;
					$nom_page = 'admin_bdd.php';							
					
					include('pagination.php');
				?>
			 </div>	
        </div>
    </div>
</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script>
var recherche_nom     = document.getElementById('nom'),
	recherche_prenom  = document.getElementById('prenom'),
	recherche_societe = document.getElementById('nom_societe/sport'),
    previousValue     = recherche_nom.value,
    previousValue2    = recherche_nom.value,
    previousValue3    = recherche_societe.value,
	
	table = document.getElementById('liste');
	pagination = document.getElementById('contient_pagination');
	
	
var resultat = document.createElement('div');
resultat.id = "resultat_recherche";
var bloc = document.getElementById('clients_recherche');
bloc.appendChild(resultat);

// pour la recherche par nom !	
	recherche_nom.onkeyup = function(e) { 		
	
		e = e || window.event; //pour IE
		
		function getResults(keywords)
		{
			
			// On lance la requête ajax
			$.getJSON('liste_membre_ajax.php?recherche='+keywords+'&type=nom&admin', function(data) {
					
				$("#resultat_recherche").empty();
				$("#resultat_recherche").html(data['tableau_client']);
			});		
		}

        if (recherche_nom.value != previousValue) { // Si le contenu du champ de recherche a changé

            previousValue = recherche_nom.value;
			previousValue = previousValue.replace(/^\s*|\s*$/g,"");
			if (previousValue != '')
				getResults(previousValue); // On stocke la nouvelle requête

        }
		
		var div = document.getElementsByTagName('div'),
		divTaille = div.length;
		previousValue = previousValue.replace(/^\s*|\s*$/g,"");
		for (var i = 0 ; i < divTaille ; i++) {
			if (previousValue != '')
			{
				pagination.style.display = 'none';	
				table.style.display = 'none';
			}
			else if (!previousValue)
			{
				pagination.style.display = 'block';	
				table.style.display = 'block';
				$("#resultat_recherche").empty();
			}
		}			

	};		
// pour la recherche par prenom !	
	recherche_prenom.onkeyup = function(e) { 		
	
		e = e || window.event; //pour IE
		
		function getResults(keywords)
		{
			
			// On lance la requête ajax
			$.getJSON('liste_membre_ajax.php?recherche='+keywords+'&type=prenom&admin', function(data) {
					
				$("#resultat_recherche").empty();
				$("#resultat_recherche").html(data['tableau_client']);
			});		
		}

        if (recherche_prenom.value != previousValue2) { // Si le contenu du champ de recherche a changé

            previousValue2 = recherche_prenom.value;
			previousValue2 = previousValue2.replace(/^\s*|\s*$/g,"");
			if (previousValue2 != '')
				getResults(previousValue2); // On stocke la nouvelle requête

        }
		
		var div = document.getElementsByTagName('div'),
		divTaille = div.length;
		previousValue2 = previousValue2.replace(/^\s*|\s*$/g,"");
		for (var i = 0 ; i < divTaille ; i++) {
			if (previousValue2 != '')
			{
				pagination.style.display = 'none';	
				table.style.display = 'none';
			}
			else if (!previousValue2)
			{
				pagination.style.display = 'block';	
				table.style.display = 'block';
				$("#resultat_recherche").empty();
			}
		}					

	};	
// pour la recherche par societe !	
	recherche_societe.onkeyup = function(e) { 		
	
		e = e || window.event; //pour IE
		
		function getResults(keywords)
		{
			
			// On lance la requête ajax
			$.getJSON('liste_membre_ajax.php?recherche='+keywords+'&type=nom_societe&admin', function(data) { 
					
				$("#resultat_recherche").empty();
				$("#resultat_recherche").html(data['tableau_client']);
			});		
		}

        if (recherche_societe.value != previousValue3) { // Si le contenu du champ de recherche a changé

            previousValue3 = recherche_societe.value;
			previousValue3 = previousValue3.replace(/^\s*|\s*$/g,"");
			if (previousValue3 != '')
				getResults(previousValue3); // On stocke la nouvelle requête

        }
		
		var div = document.getElementsByTagName('div'),
		divTaille = div.length;
		previousValue3 = previousValue3.replace(/^\s*|\s*$/g,"");
		for (var i = 0 ; i < divTaille ; i++) {
			if (previousValue3 != '')
			{
				pagination.style.display = 'none';	
				table.style.display = 'none';
			}
			else if (!previousValue3)
			{
				pagination.style.display = 'block';	
				table.style.display = 'block';
				$("#resultat_recherche").empty();
			}
		}					

	};	
</script>

<?php 

    if ($_SESSION["from_index"] != true) 
    {
        include 'footer.php';
    }

    $_SESSION["from_index"] = false;

?>