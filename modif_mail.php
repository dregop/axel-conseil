<?php 
    include 'header.php';

    if (!isset($_SESSION['identifiant'])) header('Location: index.php');

    $_SESSION["current_form"] = "mails";

    // Fonction pour upload le fichier
    function upload($file,$dossier = "fichier_mail",$put_slug,$multi = 0)
    {

        if ($multi == 1) 
        {
            $fileError = $file["error"][0];
            $fileName = $file["name"][0];
            $fileType = $file["type"][0];
            $fileSize = $file["size"][0];
            $fileTmp_name = $file["tmp_name"][0];
        }
        else
        {
            $fileError = $file["error"];
            $fileName = $file["name"];
            $fileType = $file["type"];
            $fileSize = $file["size"];
            $fileTmp_name = $file["tmp_name"];
        }

        // Tests relatifs à l'image uploadée :
        if ($fileError > 0)
        {
              //echo "Error: " . $fileError . "<br>";
              return false;
        }

        $extension_upload = strtolower(substr(strrchr($fileName,'.'),1));

        // Début protocole réstrictions de l'image :

        $allowedExts = array("gif", "jpeg", "jpg", "png","pdf"); // autorise uniquement des images
        $temp = explode(".", $fileName); // on extrait le nom de l'image
        $extension = end($temp); 

        if ($fileError > 0)
        {
            //echo "Return Code: " . $fileError . "<br>";
            return false;
        }
        else
        {

            unlink($dossier."/". $put_slug.".".$extension_upload);

            if (!is_dir($dossier)) // On test si le dossier existe ou pas
            {
                //sinon on le créer
                if (!mkdir($dossier,0777,TRUE)) 
                {
                    die('Echec lors de la création des répertoires...');
                    return false;
                }
            }
            move_uploaded_file($fileTmp_name,$dossier."/".$put_slug.".".$extension_upload);

            return true;
        }
    }

    // On récupère le profil du mail que l'on veut Modifier
    if (isset($_POST["statut"])) // On arrive sur la page 
    {
        $_SESSION["current_mail"] = $_POST["statut"];

        $requete = $bdd->prepare('SELECT * FROM mail WHERE profil = :statut')
                            or die(print_r($bdd->errorInfo()));
        $requete->execute(array(':statut' => $_POST["statut"]));
        $row = $requete->fetch(PDO::FETCH_ASSOC) ;

        if($row["profil"] != $_POST["statut"])
        {
            // Le mail n'existe pas 
            // donc les variables sont initialisés
            $_SESSION["mail_edit"] = "create";
            $_SESSION["statut"] = $_POST["statut"];

            $_SESSION["mail_data"]["content"]    = "";
            $_SESSION["mail_data"]["attachment"] = "";
            $_SESSION["mail_data"]["profil"]     = $_POST["statut"];
        }
        else
        {
            // Le mail existe : modification du mail
            $_SESSION["mail_edit"] = "edit";
            $_SESSION["mail_data"] = $row;
            $_SESSION["statut"] = $_POST["statut"];
        }

        $requete->closeCursor();
    }
    elseif (isset($_POST["upload_file"]))  // On upload 
    {
        // On upload le fichier
        upload($_FILES["file"],"fichier_mail","fichier_".$_SESSION["statut"]);
        $date = date("Y-m-d H:i:s");

        // Récupération du contenus du mail
        $content   = htmlentities($_POST["editor1"]);

        $extention = ".".strtolower(substr(strrchr($_FILES["file"]["name"],'.'),1));
        $file_name = "fichier_".$_SESSION["statut"].$extention;

        if ($_SESSION["mail_edit"] == "edit") {

            $requete = $bdd->prepare("UPDATE mail SET content = :content, attachment = :file_name , date_upload = :up_date WHERE profil = :statut ")
                or die(print_r($bdd->errorInfo()));

        }
        elseif ($_SESSION["mail_edit"] == "create") {

            $requete = $bdd->prepare("INSERT INTO `axel_conseil`.`mail` (`id`, `profil`, `content`, `attachment`, `date_upload`) VALUES ('NULL', :statut, :content, :file_name, :up_date )")
                or die(print_r($bdd->errorInfo()));
        }

        $requete->execute(array(':statut' => $_SESSION["statut"] , ':content' => $content , ':file_name' => $file_name, ':up_date' => $date));
        $requete->closeCursor();

        $_SESSION["mail_edit"] = "edit";
    }
    elseif (isset($_POST["modif_mail"])) // On modifie le mail
    { 

        $content   = htmlentities($_POST["editor1"]);
        $statut    = $_SESSION["statut"];
        $_SESSION["mail_data"]["content"] = $_POST["editor1"];

        if ($_SESSION["mail_edit"] == "edit") { /* Modification du mail */

            $requete = $bdd->prepare("UPDATE mail SET content = :content WHERE profil = :statut ")
            or die(print_r($bdd->errorInfo()));
        }
        elseif ($_SESSION["mail_edit"] == "create") { /* Creation du mail */

            $requete = $bdd->prepare('INSERT INTO mail (id, profil, content)  VALUES (NULL, :statut , :content)' )
            or die(print_r($bdd->errorInfo()));
        }

        $requete->execute(array(':content' => $content,':statut' => $statut));
        $requete->closeCursor();

        $_SESSION["mail_edit"] = "edit";
    }

?>

<section style="background-color:#7dbdff;" >
    <div class="container" style="max-width: 80rem !important;">
        <div class="row">
            <?php include "nav_deconnexion.php" ?>
            <div class="col-sm-3" style="background-color: #9f9f9f;">
			    <?php include "navbar_admin.php" ?>
			</div>
            <div class="col-sm-9">
                <h1 style="text-align:center;">Profil : <?php echo($_SESSION["current_mail"]) ?></h1>
                <div class="form-group" id="form" style="background-color: #a7d2ff; color:black; padding: 10px; margin-bottom: 0;">
                <p><?php 

                if ($_SESSION["mail_edit"] == "create") {
                     
                     echo("Aperçus non disponible , le mail n'existe pas encore.");
                }
                else {

                    echo(html_entity_decode($_SESSION["mail_data"]["content"]));
                } 

                ?></p>
                </div>
            </div>
            <div class="col-sm-12" style="margin-bottom: 15px;">
                <table class="table table-striped" id="liste" style="background-color: #a7d2ff; margin-top: 20px;" >
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nom du fichier :</th>
                          <th>Date d'ajout du fichier :</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">1</th>
                          <td><?php echo($_SESSION["mail_data"]["attachment"]); ?></td>
                          <td><?php echo($_SESSION["mail_data"]["date_upload"]); ?></td>
                        </tr>
                      </tbody>
                </table>
            </div>
            <div class="col-sm-12">
                <div class="form-group" id="form" style="background-color: #a7d2ff; padding: 10px; margin-bottom:15px;">
                    <form enctype="multipart/form-data" action="modif_mail.php" method="post">
                                        <div class="row">
                        <div class="col-sm-6">
                            <input class="btn btn-primary" style="margin:0px; width: 100%;" type="file" name="file" aria-invalid="false">
                        </div>
                        <div class="col-sm-6">
                            <input type="submit" name="upload_file" style="padding: 5px; margin-bottom:15px;" class="btn btn-lg btn-block btn-primary" value="Téléchager le fichier" />
                        </div>
                    </div>
                        <textarea rows="20" cols="50" name="editor1" >
                            <?php

                                if (isset($_POST["statut"]))
                                {
                                    echo($_SESSION["mail_data"]["content"]);
                                }
                                else
                                {
                                    echo $_POST["editor1"];
                                } 

                            ?>
                        </textarea>
                        <script> CKEDITOR.replace( 'editor1' );</script>
                        <input type="submit" name="modif_mail" style="margin-top:10px;" class="btn btn-lg btn-block btn-primary" value="Modifier le mail" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<?php 
    include 'footer.php';
?>