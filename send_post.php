<?php

include 'header.php';

if (isset($_POST['mail']))
{
	$requete = $bdd->prepare('SELECT * FROM client WHERE id = :id')
							or die(print_r($bdd->errorInfo()));
	$requete->execute(array('id' => $_SESSION['id_client']))
							or die(print_r($bdd->errorInfo()));
	$donnees = $requete->fetch();
	$requete2 = $bdd->prepare('SELECT * FROM profil_client WHERE id_client = :id')
							or die(print_r($bdd->errorInfo()));
	$requete2->execute(array('id' => $_SESSION['id_client']))
							or die(print_r($bdd->errorInfo()));
	$donnees2 = $requete2->fetch();

	$profil = $donnees2["i_e"].$donnees2["n_s"].$donnees2["t_f"].$donnees2["p_j"];

	$requete3 = $bdd->prepare('SELECT * FROM mail WHERE profil = :profil')
							or die(print_r($bdd->errorInfo()));
	$requete3->execute(array('profil' => $profil))
							or die(print_r($bdd->errorInfo()));
	$donnees3 = $requete3->fetch(MYSQL_ASSOC);
	
	// Traitement pour l'envoie du mail
	$mail = $donnees['email']; // adresse de destination.
	if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) // filtre les serveurs qui rencontrent des bogues.
	{
		$passage_ligne = "\r\n";
	}
	else
	{
		$passage_ligne = "\n";
	}

	//=====Déclaration des messages au format texte et au format HTML.
	$message_html = "<html><head></head><body><p>Mail contenant vos informations : </p>
		<h3>Informations</h3>".$passage_ligne.html_entity_decode($donnees3["content"]).$passage_ligne." Nom : ";
		if (!empty($donnees['nom']))$message_html            .= $donnees['nom'];   
		$message_html .= "<br> | Prenom : ";
		if (!empty($donnees['prenom'])) $message_html        .= $donnees['prenom'];
		$message_html .= "<br> Email : ";
		if (!empty($donnees['email'])) $message_html         .= $donnees['email']; 
		$message_html .= "<br> | Telephone : ";
		if (!empty($donnees['telephone'])) $message_html     .= $donnees['telephone'];     else $message_html .= 'Non renseigné.';
		$message_html .= "<br> Statut : ";
		if (!empty($donnees['statut'])) $message_html        .= $donnees['statut'];
		$message_html .= "<br> | Sport : ";
		if (!empty($donnees['sport'])) $message_html         .= $donnees['sport'];         else $message_html .= 'Non renseigné.';
		$message_html .= "<br> Société : ";
		if (!empty($donnees['nom_societe'])) $message_html   .= $donnees['nom_societe'];   else $message_html .= 'Non renseigné.'; 
		$message_html .= "<br> | Poste : ";
		if (!empty($donnees['poste_societe'])) $message_html .= $donnees['poste_societe']; else $message_html .= 'Non renseigné.';
		$message_html .= "<br>  Departement : ";
		if (!empty($donnees['departement'])) $message_html   .= $donnees['departement'];   else $message_html .= 'Non renseigné.'; 
		$message_html .= "<br>  Age : ";
		if (!empty($donnees['age'])) 
		$message_html           .= $donnees['age'];           else $message_html .= 'Non renseigné.'; 
		
		$message_html .= "<h3>Test Profil</h3>";
		if (!empty($donnees2['i_e'])) $message_html .= $donnees2['i_e']; else $message_html .= 'Non renseigné.';
		if (!empty($donnees2['n_s'])) $message_html .= $donnees2['n_s']; else $message_html .= 'Non renseigné.'; 
		if (!empty($donnees2['t_f'])) $message_html .= $donnees2['t_f']; else $message_html .= 'Non renseigné.'; 
		if (!empty($donnees2['p_j'])) $message_html .= $donnees2['p_j']; else $message_html .= 'Non renseigné.';
					
	$message_html .= "</body></html>";
	 
	$message_html = $message_html;
	//=====Création de la boundary
	$boundary = "-----=".md5(rand());
	$boundary_alt = "-----=".md5(rand());

	$sujet = "Vos informations client";
	 
	//=====Création du header de l'e-mail.
	$header = "From: \"Axel Conseil\"<you@yourdomain>".$passage_ligne;
	$header.= "Reply-to: \"Axel Conseil\"<you@yourdomain>".$passage_ligne;
	$header.= "MIME-Version: 1.0".$passage_ligne;
	$header.= "Content-Type: multipart/mixed;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
	 
	//=====Création du message.
	$message = $passage_ligne."--".$boundary.$passage_ligne;
	$message.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary_alt\"".$passage_ligne;
	$message.= $passage_ligne."--".$boundary_alt.$passage_ligne;


	$message.= "Content-Type: text/html; charset=\"UTF-8\"".$passage_ligne;
	$message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
	$message.= $passage_ligne.$message_html.$passage_ligne;

	$message.= $passage_ligne."--".$boundary_alt."--".$passage_ligne;

	$message.= $passage_ligne."--".$boundary.$passage_ligne;

	// récupératiion du fichier
	$fichier_from = './fichier_mail/'.$donnees3["attachment"];
	$fichier=file_get_contents($fichier_from,FILE_USE_INCLUDE_PATH);

	/* On utilise aussi chunk_split() qui organisera comme il faut l'encodage fait en base 64 pour se conformer aux standards */
	$fichier=chunk_split(base64_encode($fichier));

	//=====Ajout de la pièce jointe.
	$message.= "Content-Type: multipart/mixed; name=\"".$donnees3["attachment"]."\"".$passage_ligne;
	$message.= "Content-Transfer-Encoding: base64".$passage_ligne;
	$message.= "Content-Disposition: attachment; filename=\"".$donnees3["attachment"]."\"".$passage_ligne;
	$message.= $passage_ligne.$fichier.$passage_ligne.$passage_ligne;
	$message.= $passage_ligne."--".$boundary."--".$passage_ligne; 

	//=====Envoi de l'e-mail.
	mail($mail,$sujet,$message,$header);

	header('Location: form_profil.php?donnees_envoyees');
}

include 'footer.php';