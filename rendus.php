<?php 
    include 'header.php';

	if (!isset($_SESSION['identifiant'])) header('Location: index.php');
	
	if (isset($_POST['P']) OR isset($_POST['J']))
	{
		if (isset($_POST['P']))
			$profil = $_POST['P'];
		else
			$profil = $_POST['J'];
			
		if (isset($_SESSION['id_client']))
		{
			$requete = $bdd->prepare('SELECT id FROM profil_client WHERE id_client = :id')
									or die(print_r($bdd->errorInfo()));
			$requete->execute(array('id' => $_SESSION['id_client']))
									or die(print_r($bdd->errorInfo()));
			$donnees = $requete->fetch();			
			
			if (isset($donnees['id']))
			{
				$req = $bdd->prepare('UPDATE profil_client SET p_j=:p_j,date_test=NOW() WHERE id_client=:id');
				$req->execute(array('p_j' => $profil,
									'id' => $_SESSION['id_client']));
				$req->closeCursor(); 
			}
			else
			{
				$req = $bdd->prepare('INSERT INTO profil_client(id_client,p_j,date_test) 
									VALUES(:id,:p_j, NOW())');	
				$req->execute(array('id' => $_SESSION['id_client'],
									'p_j' => $profil));	
				$req->closeCursor();
			}
			
		}
	}
	
    $_SESSION["current_form"] = "rendus";
?>
<section>
<div class="container" style="max-width: 80rem !important;">
  <div class="row-fluid">
    <?php include "nav_deconnexion.php" ?>
    <div class="col-sm-3" style="background-color: #9f9f9f;">
        <?php include "navbar_profil.php" ?>
    </div>
    <div class="col-sm-9">
        <div class="row-fluid">
            <div class="row-fluid">
                <form class="form-horizontal" id="form2" method="post" action="test_motivation.php">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-lg btn-block btn-primary">Test de Motivation</button>
                    </div>
                </form>
                <form class="form-horizontal" id="form2" method="post" action="send.php">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-lg btn-block btn-primary">Envoyer les données par mail au client</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
  </div>
</div>
</section>
<?php 
    include 'footer.php';
?>