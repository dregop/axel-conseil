<?php 
    include 'header.php';
	
	if (!isset($_SESSION['identifiant'])) header('Location: index.php');
	
	if (!isset($_SESSION['client_email'])) header('Location: form_profil.php?new');	
	
	if (isset($_POST['projection']))
	{	
		if (isset($_SESSION['id_client'],$_SESSION['modifier_client']))
		{
			
			$requete = $bdd->prepare('SELECT id FROM motivation_client WHERE id_client = :id')
									or die(print_r($bdd->errorInfo()));
			$requete->execute(array('id' => $_SESSION['id_client']))
									or die(print_r($bdd->errorInfo()));
			$donnees = $requete->fetch();			
			
			if (isset($donnees['id']))
			{
				$req = $bdd->prepare('UPDATE motivation_client 
				SET projection=:projection,competition=:competition,relationnel=:relationnel,date_test=NOW() WHERE id_client=:id');
				$req->execute(array('projection' => $_POST['projection'],
									'competition' => '',
									'relationnel' => '',
									'id' => $_SESSION['id_client']));
				$req->closeCursor(); 
			}
			else
			{
				$req = $bdd->prepare('INSERT INTO motivation_client(id_client,projection,date_test) 
									VALUES(:id,:projection, NOW())');	
				$req->execute(array('id' => $_SESSION['id_client'],
									'projection' => $_POST['projection']));	
				$req->closeCursor();
			}
			
		}
		elseif (isset($_SESSION['creer_client'],$_SESSION['client_email']))
		{
			$requete = $bdd->prepare('SELECT id FROM client WHERE email = :email')
									or die(print_r($bdd->errorInfo()));
			$requete->execute(array('email' => $_SESSION['client_email']))
									or die(print_r($bdd->errorInfo()));
			$donnees = $requete->fetch();	
			
			if (isset($donnees['id']))
			{			
				$req = $bdd->prepare('INSERT INTO motivation_client(id_client,projection,date_test) 
									VALUES(:id,:projection, NOW())');	
				$req->execute(array('id' => $donnees['id'],
									'projection' => $_POST['projection']));	
				$req->closeCursor();
					
				$_SESSION['id_client'] = $donnees['id'];
			}
		}
		else
			header('Location: deconnexion.php');
	}
	elseif(isset($_POST['competition']))
	{
		if (isset($_SESSION['id_client'],$_SESSION['modifier_client']))
		{
			$requete = $bdd->prepare('SELECT id FROM motivation_client WHERE id_client = :id')
									or die(print_r($bdd->errorInfo()));
			$requete->execute(array('id' => $_SESSION['id_client']))
									or die(print_r($bdd->errorInfo()));
			$donnees = $requete->fetch();			
			
			if (isset($donnees['id']))
			{
				$req = $bdd->prepare('UPDATE motivation_client 
				SET competition=:competition,projection=:projection,relationnel=:relationnel,date_test=NOW() WHERE id_client=:id');
				$req->execute(array('competition' => $_POST['competition'],
									'projection' => '',
									'relationnel' => '',
									'id' => $_SESSION['id_client']));
				$req->closeCursor(); 
			}
			else
			{
				$req = $bdd->prepare('INSERT INTO motivation_client(id_client,competition,date_test) 
									VALUES(:id,:competition, NOW())');	
				$req->execute(array('id' => $_SESSION['id_client'],
									'competition' => $_POST['competition']));	
				$req->closeCursor();
			}
			
		}
		elseif (isset($_SESSION['creer_client'],$_SESSION['client_email']))
		{
			$requete = $bdd->prepare('SELECT id FROM client WHERE email = :email')
									or die(print_r($bdd->errorInfo()));
			$requete->execute(array('email' => $_SESSION['client_email']))
									or die(print_r($bdd->errorInfo()));
			$donnees = $requete->fetch();	
			
			if (isset($donnees['id']))
			{			
				$req = $bdd->prepare('INSERT INTO motivation_client(id_client,competition,date_test) 
									VALUES(:id,:competition, NOW())');	
				$req->execute(array('id' => $donnees['id'],
									'competition' => $_POST['competition']));	
				$req->closeCursor();
					
				$_SESSION['id_client'] = $donnees['id'];
			}
		}
		else
			header('Location: deconnexion.php');
	}
	elseif(isset($_POST['relationnel']))
	{
		echo 'lol';
		if (isset($_SESSION['id_client'],$_SESSION['modifier_client']))
		{
			$requete = $bdd->prepare('SELECT id FROM motivation_client WHERE id_client = :id')
									or die(print_r($bdd->errorInfo()));
			$requete->execute(array('id' => $_SESSION['id_client']))
									or die(print_r($bdd->errorInfo()));
			$donnees = $requete->fetch();			
			
			if (isset($donnees['id']))
			{
				$req = $bdd->prepare('UPDATE motivation_client 
				SET relationnel=:relationnel,competition=:competition,projection=:projection,date_test=NOW() WHERE id_client=:id');
				$req->execute(array('relationnel' => $_POST['relationnel'],
									'competition' => '',
									'projection' => '',
									'id' => $_SESSION['id_client']));
				$req->closeCursor(); 
			}
			else
			{
				$req = $bdd->prepare('INSERT INTO motivation_client(id_client,relationnel,date_test) 
									VALUES(:id,:relationnel, NOW())');	
				$req->execute(array('id' => $_SESSION['id_client'],
									'relationnel' => $_POST['relationnel']));	
				$req->closeCursor();
			}
			
		}
		elseif (isset($_SESSION['creer_client'],$_SESSION['client_email']))
		{
			$requete = $bdd->prepare('SELECT id FROM client WHERE email = :email')
									or die(print_r($bdd->errorInfo()));
			$requete->execute(array('email' => $_SESSION['client_email']))
									or die(print_r($bdd->errorInfo()));
			$donnees = $requete->fetch();	
			
			if (isset($donnees['id']))
			{			
				$req = $bdd->prepare('INSERT INTO motivation_client(id_client,relationnel,date_test) 
									VALUES(:id,:relationnel, NOW())');	
				$req->execute(array('id' => $donnees['id'],
									'relationnel' => $_POST['relationnel']));	
				$req->closeCursor();
					
				$_SESSION['id_client'] = $donnees['id'];
			}
		}
		else
			header('Location: deconnexion.php');
	}

if (isset($_SESSION['id_client']))
{
	$requete = $bdd->prepare('SELECT * FROM motivation_client WHERE id_client = :id')
							or die(print_r($bdd->errorInfo()));
	$requete->execute(array('id' => $_SESSION['id_client']))
							or die(print_r($bdd->errorInfo()));
	$donnees = $requete->fetch();
	
?>
<section>
    <div class="container" style="max-width: 80rem !important;">
        <div class="row-fluid">
        <?php include "nav_deconnexion.php" ?>
            <div id="title" class="col-md-12">
                Récapitulatif des données enregistrées dans la base de donnée :
            </div>
			<div id="form">
				<?php 
				if (!empty($donnees['comprehension'])) echo 'Compréhension : '.$donnees['comprehension'].' <br />';
				if (!empty($donnees['ancrage']))       echo 'Ancrage : '.$donnees['ancrage'].' <br />';
				if (!empty($donnees['projection']))    echo 'Projection :  '.$donnees['projection'].' <br />';
				if (!empty($donnees['competition']))   echo 'Compétiton : '.$donnees['competition'].' <br />';
				if (!empty($donnees['relationnel']))   echo 'Relationnel : '.$donnees['relationnel'].' <br />'; 
				?>
			</div>	
            <form class="form-horizontal" id="form2" method="post" action="send_post_2.php">
                <div class="col-sm-12">
                    <button type="submit" name="mail" class="btn btn-lg btn-block btn-primary">Envoyer</button>
                </div>
            </form>			
        </div>
    </div>
</section>
<?php
}
elseif (isset($_GET['donnees_envoyees'],$_SESSION['id_client']))
{
?>
<section>
    <div class="container" style="max-width: 80rem !important;">
        <div class="row-fluid">
        <?php include "nav_deconnexion.php" ?>
            <div id="title" class="col-md-12">
                Les données ont bien été envoyés par mail au client
            </div>
            <h2 style="text-align:center; margin-bottom:10%; padding:10px;">Redirection vers la page principale dans 3 secondes.</h2>
        </div>
    </div>
</section>

<?php 
    header( "Refresh:1; url=index.php", true, 303);
}
else
	header('Location: form_profil.php?new');

   include 'footer.php';
