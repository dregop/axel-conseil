<?php 
    include 'header.php';
    if (!isset($_SESSION['identifiant'])) header('Location: index.php');

    unset($_SESSION["mail_edit"],$_SESSION["mail_data"]);

    $_SESSION["current_form"] = "mails";

?>

<section style="background-color:#7dbdff;" >
    <div class="container" style="max-width: 80rem !important;">
        <div class="row-fluid">
            <?php include "nav_deconnexion.php" ?>
            <div class="col-sm-3" style="background-color: #9f9f9f;">
			    <?php include "navbar_admin.php" ?>
			</div>
            <div class="col-sm-9">
            <form action="modif_mail.php" method="post">
               <div class="form-group" id="form" style="background-color: #a7d2ff; padding: 10px; margin-bottom:15px;">
                   <label>Sélectionner le mail à modifier pour les Profils :</label>
                   <select name="statut" class="form-control">
                     <option>INTP</option>
                     <option>INTJ</option>
                     <option>INFP</option>
                     <option>INFJ</option>
                     <option>ISTP</option>
                     <option>ISTJ</option>
                     <option>ISFP</option>
                     <option>ISFJ</option>
                     <option>ENTP</option>
                     <option>ENTJ</option>
                     <option>ENFP</option>
                     <option>ENFJ</option>
                     <option>ESTP</option>
                     <option>ESTJ</option>
                     <option>ESFP</option>
                     <option>ESFJ</option>
                   </select>
                    <input type="submit" name="modif_mail" style="margin-top:15px;" class="btn btn-lg btn-block btn-primary" value="Modifier" />
               </div>
            </form>
            <form action="modif_mail.php" method="post">
               <div class="form-group" id="form" style="background-color: #a7d2ff; padding: 10px; margin-bottom:15px;">
                   <label>Sélectionner le mail à modifier pour les Motivations :</label>
                   <select name="statut" class="form-control">
                     <option>CI_PI</option>
                     <option>CI_PE</option>
                     <option>CI_CI</option>
                     <option>CI_CE</option>
                     <option>CI_RI</option>
                     <option>CI_RE</option>
                     <option>CE_PI</option>
                     <option>CE_PE</option>
                     <option>CE_CI</option>
                     <option>CE_CE</option>
                     <option>CE_RI</option>
                     <option>CE_RE</option>
                     <option>AI_PI</option>
                     <option>AI_PE</option>
                     <option>AI_CI</option>
                     <option>AI_CE</option>
                     <option>AI_RI</option>
                     <option>AI_RE</option>
                     <option>AE_PI</option>
                     <option>AE_PE</option>
                     <option>AE_CI</option>
                     <option>AE_CE</option>
                     <option>AE_RI</option>
                     <option>AE_RE</option>
                   </select>
                    <input type="submit" name="modif_mail" style="margin-top:15px;" class="btn btn-lg btn-block btn-primary" value="Modifier" />
               </div>
            </form>
            </div>
        </div>
    </div>
</section>

<?php 
    include 'footer.php';
?>