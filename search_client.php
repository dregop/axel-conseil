<?php 
    include 'header.php';
	
	if (!isset($_SESSION['identifiant'])) header('Location: index.php');
?>

<section>
    <div class="container" style="max-width: 80rem !important;">
        <div class="row-fluid">
            <?php include "nav_deconnexion.php" ?>
            <div class="col-md-12">
                <div id="title">Rechercher un Client :</div>
						<div class="row-fluid">
							<fieldset>
							<form class="form-horizontal" id="form2" method="post" action="" style="display:inline-table; width: 100%;">
								<div class="row-fluid">
									<div class="col-sm-6">
										<div class="form-group">
										  <label for="nom">Nom :</label>
										  <input type="text" name="nom" class="form-control" id="nom" placeholder="Nom">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
										  <label for="prenom">Prénom :</label>
										  <input type="text" name="prenom" class="form-control" id="prenom" placeholder="Prénom">
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
										  <label for="nom_societe">Le nom de la Société  ou le sport :</label>
										  <input type="text" name="nom_societe" class="form-control" id="nom_societe/sport" placeholder="Nom Société">
										</div>
									</div>
								</div>
							</form>	
							
							<!-- Recherche clients ajax -->
							<div id="clients_recherche">
								<h3 id="titre_recherche">Recherche clients : </h3>
							</div>
							
							<!-- Tableau des clients -->
								<h3>Derniers clients modifiés :</h3>
							<div class="col-sd-12" id="clients">
								<table class="table table-striped" id="liste" >
									  <thead>
										<tr>
										  <th>#</th>
										  <th>Nom</th>
										  <th>Prenom</th>
										  <th>Statut</th>
										  <th>Dernière Modification</th>
										  <th></th>
										</tr>
									  </thead>
									  <tbody>
									  
									 <?php 
									 $i = 1;
									$req = $bdd->prepare('SELECT * FROM client ORDER BY date_test DESC LIMIT 0,10')
																or die(print_r($bdd->errorInfo()));
									$req->execute(array('id_to' => $_SESSION['identifiant']))
																or die(print_r($bdd->errorInfo()));
									while($donnees = $req->fetch())
									{								
										echo'
											<tr>
											  <th scope="row">'.$i.'</th>';
											  if (strlen($donnees['nom'])>10)
												echo'<td>'.substr($donnees['nom'], 0, 10).'...</td>';
											else	
												echo'<td>'.$donnees['nom'].'</td>';
											if (strlen($donnees['prenom'])>10)
												echo'<td>'.substr($donnees['prenom'], 0, 10).'...</td>';
											else	
												echo'<td>'.$donnees['prenom'].'</td>';
											  
												echo '<td>'.$donnees['statut'].'</td>';
												
										echo'	
											  <td>'.$donnees['date_test'].'</td>
											  <td><a href="form_profil.php?id='.$donnees['id'].'">
												<button class="btn btn-large btn-block btn-primary" type="submit">Modifier</button>
											  </a></td>
											</tr>';
										$i++;
									}
									if ($i == 1)
										echo 'Aucun client n\'a été  créé pour le moment.';
									?>
									  </tbody>
									</table>
								</div>
							</fieldset>
						</div>
			</div>
		</div>
	</div>
</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script>
var recherche_nom     = document.getElementById('nom'),
	recherche_prenom  = document.getElementById('prenom'),
	recherche_societe = document.getElementById('nom_societe/sport'),
    previousValue     = recherche_nom.value; 
    previousValue2    = recherche_nom.value; 
    previousValue3    = recherche_societe.value; 
	
var resultat = document.createElement('div');
resultat.id = "resultat_recherche";
var bloc = document.getElementById('clients_recherche');
bloc.appendChild(resultat);

// pour la recherche par nom !	
	recherche_nom.onkeyup = function(e) { 		
	
		e = e || window.event; //pour IE
		
		function getResults(keywords)
		{
			
			// On lance la requête ajax
			$.getJSON('liste_membre_ajax.php?recherche='+keywords+'&type=nom', function(data) { 
					
				$("#resultat_recherche").empty();
				$("#resultat_recherche").html(data['tableau_client']);
			});		
		}

        if (recherche_nom.value != previousValue) { // Si le contenu du champ de recherche a changé

            previousValue = recherche_nom.value;
			previousValue = previousValue.replace(/^\s*|\s*$/g,"");
			if (previousValue != '')
				getResults(previousValue); // On stocke la nouvelle requête

        }
		
		var div = document.getElementsByTagName('tr'),
		divTaille = div.length;
		previousValue = previousValue.replace(/^\s*|\s*$/g,"");
		for (var i = 0 ; i < divTaille ; i++) {
			if (div[i] && div[i].className == 'client'  && previousValue != '')
			{
				div[i].style.display = 'none';	
			}
			else if (div[i] && div[i].className == 'client' && !previousValue)
			{
				div[i].style.display = 'block';	
				$("#resultat_recherche").empty();
			}
		}		

	};		
// pour la recherche par prenom !	
	recherche_prenom.onkeyup = function(e) { 		
	
		e = e || window.event; //pour IE
		
		function getResults(keywords)
		{
			
			// On lance la requête ajax
			$.getJSON('liste_membre_ajax.php?recherche='+keywords+'&type=prenom', function(data) { 
					
				$("#resultat_recherche").empty();
				$("#resultat_recherche").html(data['tableau_client']);
			});		
		}

        if (recherche_prenom.value != previousValue2) { // Si le contenu du champ de recherche a changé

            previousValue2 = recherche_prenom.value;
			previousValue2 = previousValue2.replace(/^\s*|\s*$/g,"");
			if (previousValue2 != '')
				getResults(previousValue2); // On stocke la nouvelle requête

        }
		
		var div = document.getElementsByTagName('tr'),
		divTaille = div.length;
		previousValue2 = previousValue2.replace(/^\s*|\s*$/g,"");
		for (var i = 0 ; i < divTaille ; i++) {
			if (div[i] && div[i].className == 'client'  && previousValue2 != '')
			{
				div[i].style.display = 'none';	
			}
			else if (div[i] && div[i].className == 'client' && !previousValue2)
			{
				div[i].style.display = 'block';	
				$("#resultat_recherche").empty();
			}
		}		

	};	
// pour la recherche par societe !	
	recherche_societe.onkeyup = function(e) { 		
	
		e = e || window.event; //pour IE
		
		function getResults(keywords)
		{
			
			// On lance la requête ajax
			$.getJSON('liste_membre_ajax.php?recherche='+keywords+'&type=nom_societe', function(data) { 
					
				$("#resultat_recherche").empty();
				$("#resultat_recherche").html(data['tableau_client']);
			});		
		}

        if (recherche_societe.value != previousValue3) { // Si le contenu du champ de recherche a changé

            previousValue3 = recherche_societe.value;
			previousValue3 = previousValue3.replace(/^\s*|\s*$/g,"");
			if (previousValue3 != '')
				getResults(previousValue3); // On stocke la nouvelle requête

        }
		
		var div = document.getElementsByTagName('tr'),
		divTaille = div.length;
		previousValue3 = previousValue3.replace(/^\s*|\s*$/g,"");
		for (var i = 0 ; i < divTaille ; i++) {
			if (div[i] && div[i].className == 'client'  && previousValue3 != '')
			{
				div[i].style.display = 'none';	
			}
			else if (div[i] && div[i].className == 'client' && !previousValue3)
			{
				div[i].style.display = 'block';	
				$("#resultat_recherche").empty();
			}
		}		

	};	
</script>

<?php 
    include 'footer.php';
?>