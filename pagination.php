<?php
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

?>
<nav style="text-align: center;">
  <ul class="pagination">

<?php

if (isset($nbr_entrees) AND $nbr_entrees != 0)
{
	$nbr_page = ceil($nbr_entrees / $nbr_affichage); 	//diviser puis arrondir au supérieur
	
	if ($current_page >= 1 AND $current_page <= $nbr_page)
	{
		if ($nbr_page > 1)
		{
			if ($current_page - 3 > 1) // Plus de 3 pages
			{
						echo ('<li><a href="admin_bdd.php?page=1">1<span class="sr-only">
						</span></a></li>');
			}
			else
			{
				if((isset($_GET['page']) AND $_GET['page'] ==1) 
				OR !isset($_GET['page']))
				{
						echo ('<li class="active"><a href="admin_bdd.php?page=1">1<span class="sr-only">
						</span></a></li>');
				}
				else
				{
						echo ('<li><a href="admin_bdd.php?page=1">1<span class="sr-only">
						</span></a></li>');
				}
			}
			
			echo '
				</span>';
			
			$count = -10;
			
			while ($count <= 10)
			{
				$affich_page = $current_page + $count ;
				
				if ($affich_page > 1
				AND $affich_page <= $nbr_page)
				{
					if(isset($_GET['page']) AND $_GET['page'] == $affich_page)
					{
						echo ('<li class="active"><a class="active" href="admin_bdd.php?page='.$affich_page.'">'.$affich_page.'<span class="sr-only">
						</span></a></li>');
					}
					else
					{
						echo ('<li><a href="admin_bdd.php?page='.$affich_page.'">'.$affich_page.'<span class="sr-only">
						</span></a></li>');
					}
				}
				$count ++;
			}
		}
	}
	else
	{	
		echo 'Erreur de numéro de page.'; // ERREUR
	}
}

// FIN GESTION AFFICHAGE PAGES
?>
  </ul>
</nav>