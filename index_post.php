<?php
session_start();
include 'bdd.php';

if (isset($_POST['identifiant']) AND isset($_POST['mot_de_passe']))
{
	// cas administrateur
	$requete = $bdd->prepare('SELECT * FROM administrateur
							WHERE identifiant = :identifiant')
							or die(print_r($bdd->errorInfo()));
	$requete->execute(array('identifiant' => $_POST['identifiant']))
							or die(print_r($bdd->errorInfo()));
	$donnees = $requete->fetch();
	
	// si c'est un testeur
	if (empty($donnees['identifiant']))
	{
		$requete2 = $bdd->prepare('SELECT * FROM testeur
								WHERE identifiant = :identifiant')
								or die(print_r($bdd->errorInfo()));
		$requete2->execute(array('identifiant' => $_POST['identifiant']))
								or die(print_r($bdd->errorInfo()));
		$donnees2 = $requete2->fetch();
		$identifiant = $donnees2['identifiant'];
		$mot_de_passe = $donnees2['mot_de_passe'];
		$membre = 'testeur';
		$_SESSION['id_testeur'] = $donnees2['id'];
	}
	else
	{
		$identifiant = $donnees['identifiant'];
		$mot_de_passe = $donnees['mot_de_passe'];
		$membre = 'admin';
	}
	
	$mdp_hache = sha1('qw'.$_POST['mot_de_passe']); // on hache le mdp
	
	
	if ($_POST['identifiant'] == $identifiant 
	AND $mdp_hache == $mot_de_passe) 	  // si l'identifiant et le mdp sont bons 
	{
		$_SESSION['identifiant'] = $membre;

		header('Location: index.php?'.$membre);
	}
	else
	{
		$erreur_connection ='fail1';
		header('Location: index.php?erreur='.$erreur_connection.'');
	}

}
else
{	
	$erreur_connection ='fail2';
	header('Location: index.php?erreur='.$erreur_connection.'');
}