<?php 
    include 'header.php';

    $_SESSION["current_form"] = "testeurs";

	if (isset($_POST['identifiant'],$_POST['mot_de_passe'])
		AND $_POST['identifiant'] != '' AND $_POST['mot_de_passe'] != '')
	{
		$r2 = $bdd->prepare('SELECT identifiant FROM testeur 
							WHERE identifiant=:identifiant')
							or die(print_r($bdd->errorInfo()));
		$r2->execute(array('identifiant' => $_POST['identifiant']))
							or die(print_r($bdd->errorInfo()));
		$donnees = $r2->fetch();
		
		if (!isset($donnees['identifiant']))
		{
			$mdp_hache = sha1('qw' . $_POST['mot_de_passe']); // on hache le mdp
			
			$req = $bdd->prepare('INSERT INTO testeur(identifiant, mot_de_passe) 
								VALUES(:identifiant, :mdp)');
			$req->execute(array('identifiant' => $_POST['identifiant'],
								'mdp' => $mdp_hache));
			$req->closeCursor(); // Termine le traitement de la requète
			
			header('Location: admin_testeurs.php'); 
		}
	}
?>

<section style="background-color:#7dbdff;" >
    <div class="container" style="max-width: 80rem !important;">
        <div class="row-fluid">
            <?php include "nav_deconnexion.php" ?>
            <div class="col-sm-3" style="background-color: #9f9f9f;">
                <?php include "navbar_admin.php" ?>
            </div>
            <div class="col-md-9">
                <div id="title">Ajout d'un Testeur :</div>
                <form class="form-horizontal" id="form" method="post" action="ajouter_testeur.php" style="background-color: #a7d2ff;">
                    <div class="row-fluid">
                        <fieldset>
                            <div class="col-sm-6">
                                <div class="form-group">
                                  <label for="identifiant">Identifiant :</label>
                                  <input type="text" name="identifiant"  class="form-control" id="identifiant" placeholder="identifiant">
                                </div>                    
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                  <label for="mot_de_passe">Votre mot de passe  :</label>
                                  <input type="password" name="mot_de_passe" class="form-control" id="mot_de_passe" placeholder="mot de passe">
                                </div>
                            </div>
                            <button type="submit" value="ajout_testeur" class="btn btn-block btn-primary">Ajouter</button>
                        </fieldset>
                    </div>
                        <?php
                            // Messages d'erreur
                            if (isset($_GET['erreur']) AND $_GET['erreur'] == 'fail1')
                                echo '<p class="erreur_field">Un testeur existe déjà avec cet identifiant.</p>';
                            if (isset($_GET['erreur']) AND $_GET['erreur'] == 'fail2')
                                echo '<p class="erreur_field">Une erreur est survenue, veuillez rééseiller.</p>';
                        ?>
                </form>
            </div>
        </div>
    </div>
</section>

<?php 
    include 'footer.php';
?>